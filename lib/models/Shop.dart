class Shop {
  int id;
  String shopName;
  String shopAddress;
  int shopServiceId;
  String shopTypeIds;
  String shopSellsIn;
  String shopWorkDays;
  String shopOpenAt;
  String shopCloseAt;
  double lat;
  double lng;
  Null shopImage;
  String email;
  int isUp;
  String createdAt;
  String updatedAt;
  int companyId;
  int userId;
  String key;
  String showInSearch;
 static List<Shop> shops = new List<Shop>();

  Shop(
      {this.id,
      this.shopName,
      this.shopAddress,
      this.shopServiceId,
      this.shopTypeIds,
      this.shopSellsIn,
      this.shopWorkDays,
      this.shopOpenAt,
      this.shopCloseAt,
      this.lat,
      this.lng,
      this.shopImage,
      this.email,
      this.isUp,
      this.createdAt,
      this.updatedAt,
      this.companyId,
      this.userId,
      this.key,
      this.showInSearch});

  Shop.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    shopName = json['shop_name'];
    shopAddress = json['shop_address'];
    shopServiceId = json['shop_service_id'];
    shopTypeIds = json['shop_type_ids'];
    shopSellsIn = json['shop_sells_in'];
    shopWorkDays = json['shop_work_days'];
    shopOpenAt = json['shop_open_at'];
    shopCloseAt = json['shop_close_at'];
    lat = json['lat'];
    lng = json['lng'];
    shopImage = json['shop_image'];
    email = json['email'];
    isUp = json['is_up'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    companyId = json['company_id'];
    userId = json['user_id'];
    key = json['key'];
    showInSearch = json['show_in_search'];
  }

  Shop.toList(dynamic jsonList) {
//    for (int i = 0; i < jsonList.length; i++) {
      shops.addAll(jsonList);
//    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['shop_name'] = this.shopName;
    data['shop_address'] = this.shopAddress;
    data['shop_service_id'] = this.shopServiceId;
    data['shop_type_ids'] = this.shopTypeIds;
    data['shop_sells_in'] = this.shopSellsIn;
    data['shop_work_days'] = this.shopWorkDays;
    data['shop_open_at'] = this.shopOpenAt;
    data['shop_close_at'] = this.shopCloseAt;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['shop_image'] = this.shopImage;
    data['email'] = this.email;
    data['is_up'] = this.isUp;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['company_id'] = this.companyId;
    data['user_id'] = this.userId;
    data['key'] = this.key;
    data['show_in_search'] = this.showInSearch;
    return data;
  }
}
