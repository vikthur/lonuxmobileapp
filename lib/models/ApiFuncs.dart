import 'package:flutter/material.dart';
import 'package:lonux/utils/Lx_Network.dart';
import 'package:http/http.dart' as http;
import 'package:lonux/models/Shop.dart';

class ApiFuncs {
  static final http.Client client =  http.Client();
  var callme = new Lx_Net(lxClient: client);

  Future <dynamic> getSuggestions(input) async{

    var result = await callme.get('get_item_suggestions/'+input);
    return result;

  }


  Future <List> getShops(query) async{
    var res = await callme.post('get_item_shops',body:{"item": query});
    return res;

  }


}