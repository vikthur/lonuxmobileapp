import 'dart:async';

mixin Validator {
  var emailValidator = StreamTransformer<String,String>.fromHandlers(
    handleData: (email,sink){
      if(email.contains("@")){
        sink.add(email);
      }else{
        sink.addError("Email is not valid");
      }
    }
  );


  var pwValidator = StreamTransformer<String,String>.fromHandlers(
      handleData: (pw,sink){
        if(pw.length > 4){
          sink.add(pw);
        }else{
          sink.addError("Password must be more than 4 character");
        }
      }
  );
}