import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'CustomException.dart';


class Lx_Net{
  final appId = "com.example.lonux";
  final _apiKey = "270e4990de7a081088094de7753c5120a1bf7e16";
  var queryLink;
  final http.Client lxClient;
  static final baseUrl = "https://lonux.com.ng/api/";
  static final localUrl = "http://172.20.10.5/";


  Lx_Net({this.lxClient}) : assert(lxClient != null);

// query api url with your http client for data using custom link as an argument
  Future <Object> fetchCt() async {
    final url = '$baseUrl/$queryLink';
    try {
      final response = await lxClient.get(url);
      if (response.statusCode != 200) {
        throw new Exception('Error getting files');
      }
      final json = jsonDecode(response.body);
      return json;
    } finally {
      lxClient.close();
    }


}

//using get method on api
  Future<dynamic> get(String url) async {
    var responseJson;
//    var scheme =
//      {
//        'app_id': appId,
//        'app_code':_apiKey,
//      };
//    var uri = new Uri.https(baseUrl, url,scheme);
    try {
      final response = await this.lxClient.get(baseUrl+url);
      if(response.statusCode == 200){
      responseJson = _response(response);
      }
      else {
        throw Exception('Request Error: ${response.statusCode}');
      }
    } on Exception {
      rethrow;
    }
    return responseJson;
  }
//using post method on api
  Future <dynamic> post(String url, {Map headers, body, encoding}) async{
    var responseJson;

    try {
      final result = await http
          .post(baseUrl+url, body: body, headers: headers, encoding: encoding)
          .then((http.Response response) {
        final String res = response.body;
        final int statusCode = response.statusCode;

        if (statusCode < 200 || statusCode > 400 || json == null) {
          throw new Exception("Error while fetching data, Error: ${response.statusCode}");

        }
        responseJson = _response(response);
//        print(responseJson);
      });
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;

  }
  //handles the response from api
  dynamic _response(http.Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body);
//        print(responseJson);
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:

      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:

      default:
        throw FetchDataException(
            'Error occured while Communicating with Server with StatusCode : ${response.statusCode}');
    }
  }

}
