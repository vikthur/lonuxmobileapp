
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper{
  final String utable = "usertable";
  final String colId = "id";
  final String colUname = "username";
  final String pw       = "password";

  static final DatabaseHelper _instance = new DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;
  DatabaseHelper.internal();
  static Database _db;

  Future<Database> get db async {
    if(_db != null){
      return _db;
    }
    _db = await initDb();
  }

  initDb() async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentDirectory.path,"main_db.db");
    var ourDb = openDatabase(path, version: 1,onCreate: _onCreate);
  }

  void _onCreate(Database db, int newVersion) async{
    await db.execute("CREATE TABLE $utable($colId INTEGER PRIMARY KEY, $colUname TEXT, $pw TEXT)");
  }
}
