import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:lonux/models/ApiFuncs.dart';
import 'package:http/http.dart' as http;
import 'package:lonux/models/Shop.dart';
import 'package:lonux/views/searchResult.dart';

var apifunc = new ApiFuncs();

class SearchBar extends StatefulWidget {
  final bool darkTheme;

  const SearchBar({Key key, this.darkTheme}) :super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SearchBarState();
  }
}


class SearchBarState extends State<SearchBar> {

  Future <dynamic> futureRes;
  TextEditingController searchCtr = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose(){
    ctr.dispose();

  }

  setText(txt){
    setState(() {
      ctr.text = txt;
    });
  }

  TextEditingController ctr = TextEditingController();
  @override
  Widget build(BuildContext context) {
    //initiate your local variables
    Color ThemeCol = widget.darkTheme == true ? Colors.white70 : Colors.black26;
    List resp;

    return TypeAheadField(
      textFieldConfiguration: TextFieldConfiguration(
          controller: ctr,
          autofocus: false,
          style: TextStyle(color:ThemeCol),
          decoration: InputDecoration(
              hintStyle: TextStyle(color: ThemeCol),
              hintText: "Search.. ",
              fillColor: Colors.white,
              suffixIcon: IconButton(
                icon: Icon(EvaIcons.search,
                    color: ThemeCol
                ),
                color: ThemeCol,
                hoverColor: ThemeCol,
                splashColor: Colors.orange,
                onPressed: ()=>{
                  click(ctr, context)
                }
              ))),
      suggestionsCallback: (pattern) async {
        List resp;
        if(pattern.length > 1) {
          resp =  await apifunc.getSuggestions(pattern);
        }
        return resp;
      },
      itemBuilder: (context, suggestion) {
        return ListTile(
          title: Text(suggestion),
          leading: Icon(Icons.shop),
        );
      },
      onSuggestionSelected: (suggestion) {
       setText(suggestion);
      },
    );
  }
}

click(ctr,context) async {
 List resp = await apifunc.getShops(ctr.text);
 if(resp.length > 0){
   Navigator.of(context).push(MaterialPageRoute(
   builder: (context)=> new SearchResult(result: resp,query: ctr.text)
   ));
 }else{
   return _showToasterMsg(context);
 }
}

void _showToasterMsg(BuildContext context){
 final snackbar = Scaffold.of(context);
 snackbar.showSnackBar(SnackBar(
     content: Text("Problem locating nearest shop, please ensure your input is correct"),
   backgroundColor: Colors.deepOrangeAccent,
   shape: RoundedRectangleBorder(
     borderRadius: BorderRadius.all(Radius.circular(12))
   ),elevation: 40,
 ));
}