import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:lonux/views/includes.dart';


class SearchResult extends StatelessWidget {
  List <dynamic> result;
  String query ;

  SearchResult({Key key, this.result,this.query});

  @override
  Widget build(BuildContext context) {
    String searched = query == null ? "" : query;
    // TODO: implement build
    return new Scaffold(
      appBar: AppBar(
        title: Container(
          margin: EdgeInsets.only(left: 15),
          child: Image.asset(
            'images/lonux_logo.png',
            width: 100,
          ),
        ),
        backgroundColor: Colors.white,
        actions: <Widget>[
          IconButton(
            icon: Icon(EvaIcons.menu2),
            color: Colors.black26,
          )
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        color: Colors.white70,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: 200,
                  child: SearchBar(
                    darkTheme: false,
                  ),
                ),
                IconButton(icon: Icon(Icons.filter_list), onPressed: null)
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 15),
              alignment: Alignment.centerLeft,
              child: Text(
                "Result for $searched",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ),
            Expanded(
                child: ListView.builder(
              scrollDirection: Axis.vertical,
              itemBuilder: (BuildContext ctxt, int currIndex) {
                return Container(
                  margin: EdgeInsets.only(top: 10),
                  padding: EdgeInsets.only(top: 10),
                  child: new Stack(
                    key: key,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          color: Colors.white,
                          padding: EdgeInsets.only(
                              top:20,
                          ),
                          child: Card(
                              color: Colors.white,
                              child: Padding(padding: EdgeInsets.all(20),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Align(
                                          alignment: Alignment.topRight,
                                          child: FlatButton.icon(
                                            icon: Icon(EvaIcons.star,color: Colors.orange,),
                                            label: Text("4.8"),
                                          )
                                      ),
                                      Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                              height: 100,
                                              width: 100,
                                              decoration: BoxDecoration(
                                                color: const Color(0xff7c94b6),
                                                image: const DecorationImage(
                                                  image: NetworkImage("https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg"),
                                                  fit: BoxFit.cover,
                                                ),
                                                borderRadius: BorderRadius.circular(50),
                                              ),
                                            ),
                                          ]),
                                      Container(
                                        padding: EdgeInsets.all(15),
                                        alignment: Alignment.center,
                                        child: Text(result[currIndex]['shop_name'],
                                          style: TextStyle(fontWeight: FontWeight.bold,
                                              fontSize: 18 ),),
                                      ),
                                      Container(
                                          alignment: Alignment.center,
                                          child: Text(result[currIndex]['shop_address'],
                                            style: TextStyle(
                                                fontWeight: FontWeight.w200,
                                                fontSize: 15
                                            ),)
                                      ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          SizedBox(
                                              child:Card(
                                                color: Colors.white70,
                                                child: Padding(
                                                  padding: EdgeInsets.all(5),
                                                  child:Text("Distance: 10km", style:  TextStyle(fontSize: 14),),
                                                ),)
                                          ),
                                          SizedBox(
                                              child:Card(
                                                color: Colors.white70,
                                                child: Padding(
                                                  padding: EdgeInsets.all(5),
                                                  child:Text("Walk: 5min", style:  TextStyle(fontSize: 14),),
                                                ),)
                                          ),
                                          SizedBox(
                                            child: FlatButton(
                                              child: Text("View more"),
                                              textColor: Colors.orange,
                                              color: null,
                                              onPressed: ()=>{debugPrint("HELlo")},
                                              shape: RoundedRectangleBorder(),

                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ))),
                        ),
                      ),
                     Positioned(
                       left:30,
                     child: Container(
                        child: SizedBox(
                          child: Card(
                            child: Container(
                              alignment: Alignment.center,
                              child: Text(
                                "Best seller",
                              ),
                              padding: EdgeInsets.all(5),
                            ),
                            color: Colors.orange,
                          ),
                          width: 100,
                          height: 40,
                        ),
                      )
                     ),
                    ],
                  ),
                );
              },
              itemCount: result.length,
            ))
          ],
        ),
      ),
    );
  }
}
