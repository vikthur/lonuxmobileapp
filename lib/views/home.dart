import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:lonux/config/SizeConfig.dart';
import 'package:lonux/utils/Lx_Network.dart';
import 'package:lonux/views/includes.dart';



class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: new Layout(),
    );
  }
}

class Layout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      color: Colors.black87,
      alignment: Alignment.center,
      padding: EdgeInsets.all(20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 30),
            child: Text(
              "Sign in",
              style: TextStyle(color: Colors.white),
            ),
            alignment: Alignment.topRight,
          ),
          Expanded(
              child: Container(
                child: Image.asset(
                  "images/lonux_logo.png",
                  width: 100,
                ),
              )),
          SearchBar(darkTheme: true),
          Expanded(child: Container(
            alignment: Alignment.center,
            child: Text("easy selling, fast shopping, instant jobs.",
              style: TextStyle(color: Colors.white),),
          )),
          Expanded(
              child: Container(
                alignment: Alignment.bottomLeft,
                child: Row(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(EvaIcons.twitter),
                      onPressed: () {
                        debugPrint("hello icon");
                      },
                      color: Colors.white,
                    ),
                    IconButton(
                      icon: Icon(EvaIcons.facebook),
                      onPressed: () {
                        debugPrint("hello icon");
                      },
                      color: Colors.white,
                    )
                  ],
                ),
              ))
        ],
      ),
    );
  }
}





