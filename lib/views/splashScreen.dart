import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_page_indicator/flutter_page_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'home.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';



PageController myCtr = new PageController(initialPage: 0);

class IntroLonux extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      body: new PageView(
        scrollDirection: Axis.horizontal,
        controller: myCtr,
        children: <Widget>[firstPage(), secondPage(), thirdPage(context)],
      ),
    );
  }
}

class Splash extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SplashState();
  }

}

class SplashState extends State<Splash> {

  Future <Widget> isFirstAccess() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool first_timer = prefs.get('first_timer') ?? true;
    if (first_timer) {
      prefs.setBool('first_timer', false);
      Navigator.of(context).pushReplacement(
          new MaterialPageRoute(builder: (context) => new IntroLonux()));
    } else {
      Navigator.of(context).pushReplacement(
          new MaterialPageRoute(builder: (context) => new Home()));
    }
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    new Timer(new Duration(milliseconds: 200),(){
    isFirstAccess();
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      body: new Center(
        child: new Text('Loading...'),
      ),
    );
  }

}

@override
dispose(){
  myCtr.dispose();
}
Widget firstPage() {
  return Container(
      padding: EdgeInsets.all(20),
      child: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
              alignment: Alignment.bottomLeft,
              margin: EdgeInsets.only(top: 10),
              child: Image.asset(
                'images/lonux_logo.png',
                width: 100,
              )),
          Expanded(
            flex: 2,
            child: Image.asset(
              'images/shop.png',
              width: 200,
            ),
          ),
          Expanded(
              flex: 1,
              child: Column(
                children: <Widget>[
                  Container(
                    child: Text.rich(TextSpan(
                        text: "Sell ",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 35,
                        ),
                        children: [
                          TextSpan(
                              text: "your products with ease",
                              style: TextStyle(
                                  fontWeight: FontWeight.w200, fontSize: 30))
                        ])),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    child: Text(
                      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pulvinar nunc feugiat felis velit. Et tempus nulla fusce nibh commodo ridiculus ultricies sit. Urna aliquam lectus feugiat.",
                      style: TextStyle(fontWeight: FontWeight.w300),
                    ),
                  )
                ],
              )),
          Expanded(
            flex: 1,
              child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
             Container(
                 child: pageIndic(myCtr),width: 200,),
//                      Icon(Icons.arrow_forward,color: Colors.orange,)
            ],
          ))
        ],
      )));
}

Widget secondPage() {
  return Container(
      padding: EdgeInsets.all(20),
      child: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
              alignment: Alignment.bottomLeft,
              margin: EdgeInsets.only(top: 10),
              child: Image.asset(
                'images/lonux_logo.png',
                width: 100,
              )),
          Expanded(
            flex: 2,
            child: Image.asset(
              'images/buy.png',
              width: 200,
            ),
          ),
          Expanded(
              flex: 1,
              child: Column(
                children: <Widget>[
                  Text.rich(TextSpan(
                      text: "Get ",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 35,
                      ),
                      children: [
                        TextSpan(
                            text: "what you are looking for faster",
                            style: TextStyle(
                              fontWeight: FontWeight.w200,
                              fontSize: 30,
                            ))
                      ])),
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    child: Text(
                      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pulvinar nunc feugiat felis velit. Et tempus nulla fusce nibh commodo ridiculus ultricies sit. Urna aliquam lectus feugiat.",
                      style: TextStyle(fontWeight: FontWeight.w300),
                    ),
                  )
                ],
              )),
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              pageIndic(myCtr),
//                      Icon(Icons.arrow_forward,color: Colors.orange,)
            ],
          ))
        ],
      )));
}

Widget thirdPage(context) {
  return Container(
      padding: EdgeInsets.all(20),
      child: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
              alignment: Alignment.bottomLeft,
              margin: EdgeInsets.only(top: 10),
              child: Image.asset(
                'images/lonux_logo.png',
                width: 100,
              )),
          Expanded(
            flex: 2,
            child: Image.asset(
              'images/job.png',
              width: 200,
            ),
          ),
          Expanded(
              flex: 1,
              child: Column(
                children: <Widget>[
                  Text.rich(TextSpan(
                      text: "Become a",
                      style: TextStyle(
                        fontWeight: FontWeight.w200,
                        fontSize: 30,
                      ),
                      children: [
                        TextSpan(
                          text: " Worker",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 35,
                              fontWeight: FontWeight.bold),
                        )
                      ])),
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    child: Text(
                      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pulvinar nunc feugiat felis velit. Et tempus nulla fusce nibh commodo ridiculus ultricies sit. Urna aliquam lectus feugiat.",
                      style: TextStyle(fontWeight: FontWeight.w300),
                    ),
                  )
                ],
              )),
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              pageIndic(myCtr),
              IconButton(
                icon: Icon(
                  Icons.arrow_forward,
                  color: Colors.orange,
                ),
                color: Colors.orange,
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: ((context) => new Home())));
                },
              )
            ],
          ))
        ],
      )));
}

Widget pageIndic(controller) {
  return SmoothPageIndicator(
    controller: myCtr,  // PageController
    count:  3,
    effect:  WormEffect(activeDotColor: Colors.orange),  // your preferred effect
  );
}
